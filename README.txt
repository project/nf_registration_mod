/**
 * @package Registration_Mod
 * @category NeighborForge
 */
This module adds the ability to have several different membership types, each collecting different
data from the user for registration by using custom content types and the combination of nodeprofile 
and pageroute modules. The accounttypes module is recommended but not required.

--CONTENTS--
  REQUIREMENTS
  SETUP/INSTALL
  FEATURES
  USAGE (Example)
  UNINSTALL
  CREDITS
  
--REQUIREMENTS--
  DO NOT USE THE PROFILES MODULE. Strictly speaking, it shouldn't get in the way of anything, but any
  data you collect on it will not be a part of the pageroute(s) you setup.

  This module requires the use of the pageroute and nodeprofile modules. Read their respective
  documentation before trying to use this module or you'll get confused. The section on USAGE will
  explain how to use them in the context of this module's needs. The usernode module should not be
  used. Any other requirements of either the nodeprofile or pageroute modules will also naturally be
  required. See their respective documentation for more information. This module was created using the
  versions of the required modules as noted in the USAGE section. Those modules may have changed, so
  something may be broken. This module will be updated as time allows, but to ensure everything works
  as described, you can always pull older files from the CVS of Drupal.org.
  
  The auto_nodetitle module is not required, but highly suggested so your users don't have to enter in
  titles for their node profile pages. Once you enable the module, when you edit a content type, there
  is a collapsed fieldset at the top of the page called 'Automatic title generation'. Open it, click on
  'Automatically generate the node title and hide the node title field.', then open 'Replacement
  patterns'. Choose a pattern (you can string several together) and place it in the textbox labeled
  'Replacement patterns'. Save. The title field will disappear from those types.

  The accounttypes module is not required, but is recommended. One would normally think of account 
  types and membership types to be the same thing. In this case, they can be, but don't have to be.
  Account types limit the roles available to a user and can be automatically assigned via this
  module based on memebership type. See the accounttypes documentation for more information on what
  it does, along with the USAGE example below for how I use it.
  
  See FEATURES for a new feature where you can auto-assign one role at membership creation/registration.
  This will only show up if you don't use the accounttypes module. Otherwise, they'd fight each other.
  
  Also, although this module allows you to auto-assign one role per membership type, the accounttypes
  module allows you to set as many as you wish.
  
--SETUP/INSTALL--
  Before you activate this module you should probably do these things:
  1) Install the nodeprofile module.
  2) Install the pageroute module.
  3) Outline the membership types you would like to have and the data you would like to collect
    from each group of users. Identify which fields are in common and which are not.
  4) Identify which fields you would like to require, and which you don't.
  5) Create the content types based on your observations in steps 3 & 4 such that you minimize
    the number of pages, but separate required/non-required, and common/non-common items. This
    may result in one or more content types with only one field, depending on your situation.
    That's okay!!! With the pageroute wizard system, it really isn't important to jam a page full
    of fields, and in my opinion, looks cleaner anyway.

  NOTE: You must UNCHECK the publish box or these nodes will show up in such pages as 'recent posts'
        and all info will be viewable!!! Since this module, along with pageroutes, handles presentation
        of these content types when they are filled out, normal users will not be able to change the
        default publishing option. So as long as your admins don't mark any of these nodes as published,
        you shouldn't be leaking personal info.

  NOTE: If you use the auto_nodetitle module, then when you create a content type, there is a collapsed
        fieldset at the top of the page called 'Automatic title generation'. Open it, click on
        'Automatically generate the node title and hide the node title field.', then open 'Replacement
        patterns'. Choose a pattern (you can string several together) and place it in the textbox labeled
        'Replacement patterns'. Save. The title field will disappear from those types.

  6) Make sure you've setup your roles and permissions how you want them. If you use the accounttypes
    module, you will keep in mind to make roles more granular than you might otherwise. If you'd
    like to use the accounttypes module, enable and set it up at this point (or earlier).
  6a) In setting up the accounttypes module, you can either create an account type for each
    membership type (as I have) or not. It may be in your case that you would like to allow two or
    more different account types for one membership type. You may, for example have an administrator
    membership type and wish to have both a junior and senior administrator account type that could
    be assigned to that membership type. Be careful to note that this module assigns a default
    account type (if you've set that up) at both account creation, and by changing membership type
    at the mass-user edit screen. More on that below.
  7) Copy the module's files into your site's module folder per standard Drupal practices.
  8) Enable it at the module administration screen.
  9) There is no install file as no database table needs creation. The standard variables table is
    used for global settings and user data is stored in the user's 'data' field.

  NOTE: This conversation may be of use to some of you: http://drupal.org/node/165230

--FEATURES--
  Now follows an overview of each setting, where to find it, and what it does.
  1) At registration, users will first be presented with a membership type selector. After selecting
    the membership type, they will fill out the standard registration form. After that, as noted
    below in B->III, the user will travel a pageroute to fill out more information.
  2) admin/user/nf_registration_mod
    This is the primary location for the settings of this module. You should see an added menu item
    in the 'User management' section called 'Registration Membership Types'. Clicking that gets you here.
    A) On the main or 'Overview' page, you will see three (or four) tabs across the top, a blank text
      field with a button, and a blank text area and another button. The tabs are explained below. 
      I) The text field is in a table and, above, it has the label of 'Membership title'. This is where
        you type in the name of each membership type in turn. Clicking on the button 'Add membership
        type' will add the name to a global setting. You can add as many membership types as you wish
        before proceeding with the form described in the next section (II), or you can alternate
        between I and II.
      II) Once you've added a membership type, there will be an empty drop down list in the 'pageroute'
        column of that new type, along with a new button 'Add pageroute tie'. You should have already
        created your pageroutes and if you did, you can choose one here. I'm using the term 'tie' to
        indicate that a relationship has been made, usually with something from another module, that
        this module does not directly control nor infulence, but uses in some other way.
      III) The delete link in each row will of course present you with a choice to really delete that
        particular membership type or not. There's a note that says your pageroute will not be
        destroyed via this module, but that you can use the pageroute module if you intend to get rid
        of that as well.
      IV) The edit link takes you to an edit page where you can change both the name and pageroute for
        a given membership type. There is currently no method to edit multiple membership types and
        there are no plans to do such since I can't think of a use-case where more than a dozen
        membership types would be used and anything less should be easily managed as-is.
      V) The 'User instructions' text area is a place where you can enter the instructions required for
        a user to correctly choose between the membership types you are providing. This can be changed
        here directly whenever you need to. Click the 'Save message' button to save it.
      NOTE: in both III & IV above, updates to the account type ties are maintained.
    B) The next tab is 'Content type adjustments' (admin/user/nf_registration_mod/ct_adjustments).
      Because the content types you created for node profiles should not be treated as regular content
      types, this area both shows you some default changes this module makes and allows you to make
      others. The page is organized by content type. The path of each is shown, as is which pageroutes
      use it and what the pages are called in each pageroute.
      I) Actually, in looking at the screen again, I see that I didn't give you an indicator that the
        pageroute menu items are disabled, but that's what happens by default.
      II) There is a note that advises you to disable all content type links for those types related
        to the pageroutes you've setup. You check the items you want to disable and click on 'Disable
        menu items' to affect that change. This does not delete the menu item. If you were to go to
        the regular menu admin, you would see them, but they'd be disabled. This removes the 'Create
        content' menu items as well as those on the content type page when you have clicked 'Create
        content' and haven't selected a type yet. This module is aware of my 'Content type
        Administration by Organic Groups' module and removes these types from the relevant admin
        pages. It also adds these types to the OG omiited content type list which prevents groups
        from using them.
      III) If you wish to prevent users from using your site until they've completed some or all of
        a given pageroute, you can specify which content types are required. For this reason, I made
        the suggestion in the SETUP section above for how to segregate the content fields as you
        create your content types. For example, you may require a full name and an address for all
        users. Place that on one content type and require it on this page. You might have another
        content type just for corporate accounts that have other required info. Put that on another
        content type. Presumably, you'd have at least two pageroutes. One would have that first
        required type, the other would have both required types. You could then add other content
        types for non-required info. Check the boxes for the types you want to require and then click
        'Require for login'. 
        
        WHAT THIS ACTUALLY DOES is reroute users who have not made nodes of the required types to the
        next required type that they need to make. This happens both at login and any time the user
        tries to browse away using any link on the page except the logout link.
        
        Some of how this works depends on the settings you made for registration. If you require that
        a user be validated by an admin, then the user will not be able to fill these nodes out until
        they are unblocked. If you have required email verification, the user will be logged out right
        after selecting a membership type and filling out the standard registration form. They will
        be able to fill out the required nodes once they've verified their email address and come back
        to the site. If you have neither required email verification, nor admin approval, then the
        user will be able to fill out the nodes right away.
        
        If a user quits their session/logs out before completing all required content types, they will
        be presented with the next content type they need to complete and a total listing of pages to
        submit. If you use the pageroute settings as outlined in the USAGE example, this will keep your
        users from getting confused as to what is required of them.  
      NOTE: Because this screen has two buttons which use the same checkboxes, rather than check the
      boxes of previously disabled/required types for you, all checkboxes default to unchecked. This
      means that you must re-check previously affected types if you are making ANY changes or else they
      will be undone. For this reason, I've provided the 'Status' column so you can see what state each
      type is in.
      CAUTION: Hmm, I thought of one two paragraphs ago, but now can't remember.
    C) The next tab is 'Set access' (admin/user/nf_registration_mod/set_access) and is a placeholder
      for now. What I would like to do is show a slice of the permissions page so that you can assign
      only those permissions to roles that pertain directly to the content types we are dealing with
      on these particular pageroutes. A first look leads me to believe that I need to work on the full
      array of permissions. So even though I would only show a slice, I would have to keep track of
      all permissions for all roles. This would require duplicating a lot of code from the standard
      user module. I'm hoping there's a better way, so I've put this on the back burner. So instead,
      I've provided some little bit of instruction as to how to set your content type permissions
      as well as a link to the regular permissions page.
      
      To summarize how you may want to set permissions, you first need to determine which types you
      want users to edit after creation. Keep in mind that without additional access modules, that
      if a user cannot edit their own node, then they cannot view it either. In my case, I am allowing
      both 'create' and 'edit own' permissions for all content types, then restricting the individual
      fields with another access control module. You could do the same, or you could disallow 'edit
      own', then use a module like TAC to somehow give the 'view' grant only. This is a complicated
      issue, so any variations from the USAGE example below are up to the user to figure out. Feel
      free to share your particular setup with us in the issue queue.
    D) The fourth tab only shows up if you've enabled the accounttypes module. The tab is labeled
      'Tie account type to membership' (admin/user/nf_registration_mod/account_type). On this page
      you can set a default account type to be given to a user whenever their membership type
      changes to that membership type.
        I) This is a straight-forward page where each membership type is displayed and there is a
          drop down next to each to choose which account type to associate with it. Select the one
          you want and click 'Save assignments'.
        II) If you don't want all of your membership types to be associated to an account type, too
          bad. The workaround is to create an account type (or use the 'basic' one that comes with
          the accounttypes module) and then not assign it any roles, or assign it all roles -
          whichever makes sense for your situation.
    E) The edit link (admin/user/nf_registration_mod/edit/<MEMBERSHIP NAME>) for each membership type
      allows you to change the name of the membership type, associate it with a different pageroute,
      or, if you don't have the accounttypes module activated, will allow you to select one role to
      auto-assign at account creation. Each membership type can assign a different role. The
      accounttypes module allows assignment of multiple roles and so supercedes this module in that
      respect.

--USAGE--
  This outlines not only the suthor's settings for this module, but also for the nodeprofile and
  pageroute modules. This should more or less guarantee decent results.
  First let me tell you the versions of the modules' files used [not module version, but file]:
  nodeprofile.module - 1.7.2.14
  pageroute.module - 1.39.2.26
  pageroute_subform.inc - 1.1
  pageroute_ui.module - 1.29.2.6
  So figure out which releases these correspond to. I can't say for certain whether later versions
  will work the same or not as I'm not the author of them.
    1) After installing the nodeprofile module and the pageroute module, I decided to create three
      membership types. I then outlined the data I needed to collect from each. I saw that some was
      common between two or more membership types. Beginning with what was common, I further divided
      that information into required and non-required status. I then made my first content type using
      the common & required fields (PAGE2). I then moved to the common & non-required information.
      This data fit on two content types nicely (PAGE3 & PAGE4). Then I had three required but not
      common content types to create (PAGE1, PAGE1A & PAGE1B). I then determined what the flow
      through each pageroute should be, hence the page names above. The settings for all of this
      follow.
    2) The first settings to manipulate are in each content type's edit page
      (admin/content/types/YOUR_TYPE). Here, I unchecked all default options, set maximum population
      to 1, checked the 'Use this content type as a nodeprofile for users' box, disbaled comments,
      and unchecked or disabled everything else (from various modules I had enabled).
    3) After saving the changes and returning to the same edit page, I then clicked in the 'Node
      profile' tab (admin/content/types/YOUR_TYPE/nodeprofile). Here, I unchecked the 'Integrate this
      node profile with user categories.' box. This is important because this module handles this
      on its own. If you use nodeprofile's version (which does override mine) then your user will
      have a tab for every nodeprofile content type, whether it is part of their membership type
      or not. I then selected the 'Don't display this nodeprofile on the user account page' radio
      button and set the weight depending on the order of that content type within the pageroute(s)
      it would be in; the first was lightest.
    3a) I'm not sure that I needed this or not, so I'll mention this in passing. I hope this wasn't
      required since I didn't put in on the requirements list, but I did use the nodefamily module.
      Because I have three page ones (PAGE1, PAGE1a and PAGE1b), I made them each parents and then
      made each content type that would be in its pageroute a child. Because there are content
      types in common, those types appear as children to more than one parent. The admin for
      nodefamily is here: admin/content/nodefamily.
    3b) It turns out that for the version of nodeprofile I was using (and it may still be the case),
      the nodefamily module was required just to get the node population section mentioned above. Or
      am I remembering that incorrectly? Check the nodeprofile docs to be sure.
    4) Next I worked on the pageroutes at admin/build/pageroute. First thing I did there was create
      the routes at admin/build/pageroute/add using the 'add route' tab. I named the path, then
      deselected the workflow option, selected 'Show tab-like submit buttons above the page content.',
      assigned the appropriate roles to it, selected one of the PAGE1's for the nodefamily head (optional),
      set the 'Customized redirect' to user/!uid, and checked 'Preserve destination parameter during
      this route.' I recommend that you name your pages the same as the human-readable names of your
      content types. The reason is that this module gives a list of required pages to the user and
      it is nice if profile category links match pageroute submit-tab links.
      
    NOTE: It seems that perhaps pageroute wants SINGLE WORD paths for the pageroute path, so don't create
          pageroute paths like 'my/pageroute' when you setup your pageroutes!
    
    NOTE: Workflow may have been moved to a separate module.

    NOTE: For 'Customized redirect', you might want to send each pageroute to a separate URL where you've
          created a welcome page for your new user and describe for them the limitations/uses of their
          membership type.

    5) Then I added the pages to each route. That is done from the 'Add page' tab
      (admin/build/pageroute/route/YOUR_ROUTE_NUMBER/add). Because each of my content types was given
      a population maximum of one above, they are called 'lonely nodes' and so at the first screen
      of the add wizard, I selected 'Lonely node management' type. This takes you to the same page
      as what is later the page-edit page. When you edit the page later it will be found here:
      admin/build/pageroute/route/YOUR_ROUTE_NUMBER/edit/YOUR_PAGE_NAME. At this point, I gave the
      page a name, left the title blank, and checked the 'Activated' box. I skipped the 'dont show
      a tab for this page' section, selected the content type that should be associated with the
      step in the pageroute, left the cancel link label empty, labeled the 'Forward button label'
      as 'Next page' because that's what this module uses for the membership page, and labeled the
      'Back' button 'Back'. Then of the three checkboxes, I only checked 'Never display the delete
      button' because I don't want users to delete this information; I only want the admins to do
      that. Again, I set the weight according to the page order, the first being the lightest.
    6) Now, unless I'm forgetting something, that should be everything to setup content types,
      nodeprofiles, nodefamily, and pageroutes. Next is the configuration of this module.
    7)Nearly all steps I took to setup this module are obvious, so I'll direct you to the FEATURES
      section above. I will say that on the admin/user/nf_registration_mod/ct_adjustments page, I
      "disabled" all content types which removes the nodeprofile content types from menus and listings
      (since we only want them created at registration), and "required" those that I planned to be
      required right from the start. I also set the appropriate roles to have access to both 'create'
      and 'edit own' for each content type in the pageroutes. Last, I associated an account type with
      a membership type. That's it!
    If anyone has questions regarding the process above, please feel free to open a support issue.
    But like I said, variations from this recipe will result in a varied outcome.
    
--UNINSTALL--
  Because this module doesn't create or maintain its own database tables, you can disable the module
  and don't need to visit the uninstall page. Just in case you want to disable this and then reenable
  it later, I don't delete the entries this module makes in the variables table, nor do I delete the
  entries to each user's data field. I figure that for the vairables, it's easy enough for most admins
  to delete them by hand. They all start with the prefix 'nf_registration_mod'. And since I only add
  one piece of information to the user object, I figured you all wouldn't mind keeping it around. If
  I get enough complaints, I guess I'll look into a more formal disable routine.
  
--CREDITS--
  This module was created by Ryan Constantine (drupal id rconstantine)